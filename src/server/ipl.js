const fs = require('fs')



//--------- First Problem : Number of matches played per year for all the years in IPL.-------//

function NumberOfMatches(matchesIPL) {

    const yearsArray = matchesIPL.map(year => year.season);

    const yearsCount = yearsArray.reduce((totalCount, currentYear)=>{
        totalCount[currentYear] = totalCount[currentYear] ? totalCount[currentYear] +1 :1;
        return totalCount;
    },{})

    fs.writeFile('../public/output/Number_of_Matches.json', JSON.stringify(yearsCount), 'utf8',
    function(err){console.log(err);})

return (yearsCount);
}



//-------------- Second Problem : Number of matches won per team per year in IPL.---------------//

function MatchesWon(matchesIPL) {

    const uniqueYears = Object.keys(NumberOfMatches(matchesIPL))

    const matchesWon = uniqueYears.reduce((matchesWonAcc, year)=>{
        let teams = matchesIPL.filter(element => element.season == year).map(team => team.winner)
            .reduce((totalWins, currentTeam)=>{
            totalWins[currentTeam] = totalWins[currentTeam] ? totalWins[currentTeam] +1 :1;
            return totalWins;
            },{});
            matchesWonAcc[year] = teams
    
    return matchesWonAcc
    }, {})

    fs.writeFile('../public/output/Matches_Won.json', JSON.stringify(matchesWon), 'utf8',
    function(err){console.log(err);})

return matchesWon;
}



//------------- Third Problem : Extra runs conceded per team in the year 2016 --------------//

function Extra_Runs(matchesIPL, DeliveriesIPL){

    const id2016 = matchesIPL.filter(year => year.season == 2016).map(match => match.id);

    const extraRuns = DeliveriesIPL.reduce((totalRuns, current) => {
        if(id2016.includes(current.match_id)){
            if(totalRuns.hasOwnProperty(current.bowling_team)){
            totalRuns[current.bowling_team]+= +(current.extra_runs)}
            else {totalRuns[current.bowling_team]= +(current.extra_runs)}
        }
        return totalRuns;
    },{})

    fs.writeFile('../public/output/Extra_Runs.json', JSON.stringify(extraRuns), 'utf8',
    function(err){console.log(err);})

return(extraRuns);
}




//--------------- Fourth Problem : Top 10 economical bowlers in the year 2015 -------------//

function economical(matchesIPL, DeliveriesIPL){

    const id2015 = matchesIPL.filter(year => year.season == 2015).map(match => match.id);

    let givenRuns = DeliveriesIPL.reduce((player, runs) => {
        if(id2015.includes(runs.match_id)){
            if(player.hasOwnProperty(runs.bowler)){
            player[runs.bowler]+= +(runs.total_runs)}
            else {player[runs.bowler]= +(runs.total_runs)}
        }
        return player;
    },{})

    let totalBalls = DeliveriesIPL.reduce((player, balls) => {
        if(id2015.includes(balls.match_id)){
            if (balls.wide_runs == 0 && balls.noball_runs == 0) {
                player[balls.bowler] = player[balls.bowler] ? player[balls.bowler] +1 :1;
            }
        }
        return player;
    },{})

    for (let key in totalBalls) { 
    totalBalls[key] = totalBalls[key]/6; }

    for (let key in givenRuns) { 
    givenRuns[key] = givenRuns[key]/totalBalls[key]; }

    economy = Object.entries(givenRuns);
    economy.sort((a,b) => a[1] - b[1]);
    economy = economy.slice(0, 10)

    fs.writeFile('../public/output/Economical_Bowlers.json', JSON.stringify(economy), 'utf8',
    function(err){console.log(err);})

return economy;
}




//------- Exporting Functions ---------//

module.exports = {
    NumberOfMatches,
    MatchesWon,
    Extra_Runs,
    economical
}