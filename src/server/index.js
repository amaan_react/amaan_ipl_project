const ipl_deliveries= require('../data/deliveries.json')
const ipl_matches= require('../data/matches.json')

const ipl = require('./ipl');

function main() {
    console.log(ipl.NumberOfMatches(ipl_matches));
    console.log(ipl.MatchesWon(ipl_matches));
    console.log(ipl.Extra_Runs(ipl_matches, ipl_deliveries));
    console.log(ipl.economical(ipl_matches, ipl_deliveries));
}

main()