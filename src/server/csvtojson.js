const fs = require('fs')
const csv = require('csvtojson')

const matchesPath = '../data/matches.csv'
const deliveriesPath = '../data/deliveries.csv'

function Conversion(csvFilePath){
    csv().fromFile(csvFilePath).then((convertedFile)=>{
    const outPath = modifyFilePath(csvFilePath)
    fs.writeFileSync(outPath, JSON.stringify(convertedFile), 'utf8',
    function(err){console.log(err);})
})
}
const modifyFilePath = filename => filename.replace('.csv', '.json')

Conversion(matchesPath);
Conversion(deliveriesPath)

